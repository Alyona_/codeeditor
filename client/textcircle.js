this.Documents = new Mongo.Collection("documents");

if (Meteor.isClient){

	Meteor.setInterval(function(){
		Session.set("current_date", new Date());
	}, 1000);//Update the session every 1000 ms

	Template.date_display.helpers({
		current_date:function(){
			//return new Date();
			return Session.get("current_date");
		}
	})//end of date date_display helper
	Template.editor.helpers({
		docid:function(){
			console.log("I am client");
			console.log("docid helper:");
			console.log(Documents.findOne());
			//return.Documents.findOne()._id; erase this
			var doc = Documents.findOne();
			if(doc){
				return doc._id;
			}
			else {
				return undefined;
			}
		}//end of docid:function
	});//end of editor temp helpers
}

if (Meteor.isServer){
	Meteor.startup(function(){
		// code to run on server at startup

		if(!Documents.findOne()){//no docs found
				Documents.insert({title:"my new document"});
		}

	})//end of startup function
}
